import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import { initializeApp } from "firebase/app";
import moment from "moment";


 Vue.config.productionTip = false;

 
const firebaseConfig = {
  apiKey: "AIzaSyBOhxMsNk-aXIOem7n5CigP0rmSwEfjarg",
  authDomain: "eventapp-66e7f.firebaseapp.com",
  projectId: "eventapp-66e7f",
  storageBucket: "eventapp-66e7f.appspot.com",
  messagingSenderId: "1021753370178",
  appId: "1:1021753370178:web:ed1159a9df661c95f96fdb",
  measurementId: "G-NNXZ8S268S"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
Vue.filter("formatDate", function(value) {
  if (value) {
    return moment(String(value)).format(" DD MMM yyyy ,h:mm:ss ");
  }
});
 
new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')


 




 