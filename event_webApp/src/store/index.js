import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  plugins:[createPersistedState()],
  state: {
    user:{
      name:'',
      token:'',
      teamId:'',
      eventName:'',
      emailID:'',
      imageUrl:'',
      // id:''
    }
  },
  mutations: {
    setUserData:function(state,user){
      state.user.emailID=user.emailID
      state.user.token=user.token
      state.user.name=user.name
      // state.user.id=user.id
      state.user.imageUrl=user.imageUrl
      state.user.teamId=user.teamId
      state.user.eventName=user.event_name
      
     }
  },
  actions: {
  },
  modules: {
  },
  getters:{
    getUser:(state)=>{
      return state.user
    }
  }
})
