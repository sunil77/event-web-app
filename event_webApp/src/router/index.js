import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/pages/Login';
 import Auth from '../views/pages/Auth'
 import langdingPage from  '../views/LandingPage'
//  import firebase from "firebase";

Vue.use(VueRouter);

const routes = [
  {
    path: "/layout",
    name: "Layout",
    component: () => import("@/components/Layout.vue"),
    meta: {
      authRequired: true,
    },
    children: [
      {
        path: '/home',
        name: 'Home',
        component: Home,
        meta: { requiresAuth: true },
      },
       {
        path: '/history',
        name: 'History',
        component: () => import("@/views/history/History.vue"),
        // component: History,
        meta: { requiresAuth: true },
      },
      {
        path: "/history/historyteam",
        name: "HistoryTeam",
        component: () => import("@/views/history/HistoryTeam.vue"),
        meta: {
          requiresAuth: true,
        },
      },
       {
        path: '/events',
        name: 'Events',
        component: () => import("@/views/events/Events.vue"),
        meta: { requiresAuth: true },
      },  
      {
        path: '/events/currentevents',
        name: 'CurrentEvents',
        component: () => import("@/views/events/CurrentEvents.vue"),
        // component: History,
        meta: { requiresAuth: true },
      },              
    ]},
  {
    path: '/auth',
    name: 'Auth',
    component: Auth,
    children: [
      {
        path: '/login',
        name: 'Login',
        component: Login
         
      } 
    ],
  },
  {
    path: '/Landingpage',
    component: langdingPage,
    name:"landingpage"
  },
];

// const routes = [
//   {
//     path: "/",
//     name: "MainContainer",
//     redirect: "/home",
//     component: MainContainer,
//     children: [
//       {
//         path: "/home",
//         name: "Home",
//         component: Home,
//         meta: { requiresAuth: true },
//       },
//     ],
//   },
//   {
//     path: "/pages",
//     redirect: "/pages/login",
//     name: "Pages",
//     children: [
//       {
//         path: "login",
//         name: "Login",
//         component: Login,
//       },
//     ],
//   },
// ];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});
// router.beforeEach((to, from, next) => {
//   if (to.matched.some((record) => record.meta.authRequired)) {
//     if (firebase.auth().currentUser) {
//       next();
//     } else {
//       // alert('You must be logged in to see this page');
//       next({
//         path: "/",
//       });
//     }
//   } else {
//     next();
//   }
// });

export default router;
