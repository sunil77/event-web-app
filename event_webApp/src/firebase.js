import { initializeApp } from 'firebase/app';
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  signOut,
} from 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyBOhxMsNk-aXIOem7n5CigP0rmSwEfjarg',
  authDomain: 'eventapp-66e7f.firebaseapp.com',
  projectId: 'eventapp-66e7f',
  storageBucket: 'eventapp-66e7f.appspot.com',
  messagingSenderId: '1021753370178',
  appId: '1:1021753370178:web:ed1159a9df661c95f96fdb',
  measurementId: 'G-NNXZ8S268S',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
  const auth = getAuth();

export default {
  auth: getAuth(),
  async login() {
    const provider = new GoogleAuthProvider();
    let result = await signInWithPopup(auth, provider);
    const credential = GoogleAuthProvider.credentialFromResult(result);
    const token = credential.accessToken;
    const user = result.user;
    let response={
        user:user,
        token:token
    }
    // console.log(response,'9999999');
    return response
    // .then((result) => {
    //    const credential = GoogleAuthProvider.credentialFromResult(result);
    //       const token = credential.accessToken;
    //         const user = result.user;
    //         return user
    //  }).catch((error) => {
    //    const errorCode = error.code;
    //   const errorMessage = error.message;
    //    const email = error.email;
    //    const credential = GoogleAuthProvider.credentialFromError(error);
    //   // ...
    // });
  },
  logout() {
    signOut(auth)
      .then(function() {})
      .catch(function(error) {
        console.log(error);
      });
  },
};
